
# Changelog for workspace-tree-widget

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v6.35.4] - 2024-07-17

-  Updated `ws-task-executor-widget` dependency lower range [#27747#note-20]

## [v6.35.3] - 2024-03-19

- Moved to maven parent 1.2.0
- Added lombok 1.18.4 (provided) to avoid `java.lang.ClassNotFoundException: com.sun.tools.javac.code.TypeTags`
- Equipped with the new Catalogue Publishing Widget [#26825]
- image-previewer-widget range at [1.0.0, 1.3.0-SNAPSHOT)

## [v6.35.2] - 2022-09-14

#### Bug fixed

- [#23676] Fixed - Added extension .txt to ASC files when dowloaded 
- [#23789] Fixed - Notebook files (.ipynb) are downloaded with .txt attached.
- [#23862] Fixed - Downloading folders containing a "." in the name, the .zip is appended to file name

## [v6.35.1] - 2022-06-27

- [#23523] Updated to maven-portal-bom 3.6.4

## [v6.35.0] - 2022-05-03

#### Enhancements

- [#23225] Updated the method to read the members of (VRE or Simple) shared folders

## [v6.34.3] - 2022-03-24

#### Enhancements

- [#23020] Reinforce the (ApplicationProfile-)Workspace-Explorer-App discovery
- [#22923] Migrate to maven-portal-bom 3.7.0[-SNAPSHOT]

## [v6.34.1] - 2021-12-20

#### Fixes

- [#22578] GetInfo facility invoked by Tree View does not display properly the Owner field

## [v6.34.0] - 2021-11-05

#### Enhancements

- [#22251] Make workspace file size field smart

## [v6.33.1] - 2021-06-11

#### Fixes

[#21575] Fixed icon associated with simple folders in the grid view
Moved to maven-portal-bom 3.6.3

## [v6.33.0] - 2021-05-11

#### Enhancements

[#21388] Integrated with new workflow to interact with ws-thredds 1.x 
[#21444] Moved to maven-portal-bom >= 3.6.2-SNAPSHOT 

## [v6.32.0] - 2021-04-12

#### Enhancements

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20762] Moved to ckan-metadata-publisher-widget 2.X

## [v6.31.3] - 2021-03-25

#### Bug Fixes

Declared the workspace-uploader range [2.X, 3.X-SNAPSHOT)

## [v6.31.2] - 2021-01-18

#### Bug Fixes

[#20457] Just including patched library

## [v6.31.1] - 2020-10-19

#### Bug Fixes

[#19952] Fixed incident

## [v6.31.0] - 2020-09-29

#### Enhancements

[#19600] revisit the "Get Info" Dialog in a modern view

#### New Features

[#19695] Show the file preview via Google Docs Viewer

#### Bug Fixes

[#19759#note-12] Fixed updating the description of a folder

## [v6.30.1] - 2020-06-25

#### Fixes

[#19544] update the unsharing messages in the accounting history


## [v6.30.0] - 2020-05-18

#### New Features

[#19058] Restore operation: the user has to select the destination folder

#### Fixes

[#19232] Fixed upload of file names that use special characters

[#19243] Fixed upload archive facility does not work properly with Windows OS


## [v6.29.0] - 2020-03-27

#### New Features

[#18150] Get Shareable Link as Long URL also

[#18150] Get Shareable Link copy to clipboard facility

[#18174] Workspace Search facility, business logic applied on workspace side

#### Fixes

[#18577] Fixing Shareable link informative text for public file

[#18926] Workspace portlet: previous version file download errors


## [v6.28.1] - n.a.

Migrated to git


## [v6.28.0] - n.a.

Merged with branch version

Changed shared folder check to send notification


## [v6.27.0] - 2019-09-25

[Task #17226] Workspace and Workspace Tree: migrate HL remaining methods to SHUB

Updated the list of classes shared with sharing widget component

[Task #17552] Workspace: reconsider the actions Private Link, Folder Link and Get Link


## [v6.26.0] - 2019-08-02

[Task #17135] Workspace: assign the file extension during the download

[Feature #17091] Reconsider error message when deleting a shared folder


## [v6.25.1] - 2019-07-05

[Task #17018] Move some methods to SHUB due to backward compatibility broken between SHUB and HL


## [v6.25.0] - 2019-04-04

Updated to StorageHub [ticket: #13226]

[Support #16430] fixed

[Task #12910] Rename and delete operations on shared folder should be managed by dedicated message on WS-side


## [v6.24.1] - 2019-01-08

Updated regular expression to validate Folder and File names

[Task #12911] Called getFullName for any user other than logged user

[Task #13106] Provide public links for versioned files


## [v6.23.0] - 2018-11-13

[Task #12521] Migrate the tree view

[Task #12504] Migrate the Grid View to StorageHub

[Task #12556] Migrate the Download facility to Storage Hub

[Task #12504] Migrate Create folder facility and its notifications to StorageHub

[Task #12601] Migrate Download Folder facility to StorageHub

[Task #12604] Migrated Move operation to StorageHub

[Task #12603] Migrated Copy operation to StorageHub

[Task #12603] Migrated Rename facility to StorageHub

[Task #12603] Migrated Public Link facility to StorageHub

[Task #12664] Migrated Versions facility to StorageHub

[Incident #12923] Workspace public folders are shown as regular shared folders

[Incident #12922] Workspace get link not working


## [v6.22.0] - 2018-09-14

[Task #12489] Migrate delete operation to StorageHub

integrated CLARIN Switchboard with service endpoint query for Switchboard service discovery


## [v6.21.1] - 2018-07-02

[Task #12089] Migrate to workspace-uploader 2.0

[Release #12006] Removed Send to


## [v6.21.0] - 2018-06-07

[Project Activity #11690] Integrated with Task Executor Widget


## [v6.20.1] - 2018-04-19

Managed case of Exception in order to avoid failure on checkItemLocked


## [v6.20.0] - 2018-03-01

[Task #11127] Porting to new ws-thredds engine

[Feature #11325] Workspace: add New URL feature in the context menu of right pane


## [v6.19.0] - 2018-01-09

Issue #10831, Workspace download folder tomcat temp occupation issue


## [v6.18.0] - 2017-09-29

Feature #9760: WS-Thredds synch folder integration


## [v6.17.2] - 2017-09-13

Incident #9676: fixed. Removed check on get sub-folder public link when operation is performed by an administrator


## [v6.17.1] - 2017-07-10

Added regex in order to remove list of chars when renaming or creating new item. It is different for folder and item name


## [v6.17.0] - 2017-05-22

[Feature #5207] Integrate Image Preview Widget


## [v6.16.1] - 2017-04-11

Quick fix to set folder as public

Updated messages to history including version of file if it is present


## [v6.16.0] - 2017-03-03

Removed no longer used dependency: accesslogger

[Feature #7006] File Versioning

Edit Administrator can be performed by other Administrator/s

[Task #7382] Added a quick fix to 'Edit Permissions'

'Edit Permissions' can be performed by Administrators


## [v6.15.2] - 2016-01-20

Added a loader on share window when contacts are loading from server

Removed currUserId parameter from client side required for external servlets


## [v6.15.1] - 2016-01-05

Removed message box shown in case of failure on getAllContatcs


## [v6.15.0] - 2016-11-29

[Feature #5873] Remove ASL Session from the Workspace and its components

Updated logic to get Folder Link (as Public folder). The access is granted for owner or admin


## [v6.14.0] - 2016-09-29

[Feature #2335] Added "Get Folder Link" facility

[Incident #4878] Fixed: Workspace tree not displaying user's name

Added 'gcube-url-shortener' dependency, removed internal classes for shortener

[Feature #5116] Implemented Show public folders in Workspace via Smart Folder

[Feature #5110] Added Enabled/Disabled Public Access to workspace history

[Bug #5218] Fixed return to the Workspace root Folder


## [v6.13.0] - 2016-05-31

[Feature #4128] Migration to Liferay 6.2

Data Catalogue publishing supported


## [v6.12.0] - 2016-05-16

Integrated with new workspace uploader

Bug fixed: Css for Dialog Cancel multiple files


## [v6.11.1] - 2016-02-24

Added icons for: .odt, .ods, .ott, .odg, .odp


## [v6.11.0] - 2016-01-20

[Feature #1925] Added, new public link format: http://host/storageID

[Feature #1298] Update public link generation


## [v6.10.1] - 2015-12-16

Bug Fixing - #1804; #1808; #1822; #1333


## [v6.10.0] - 2015-11-19

[Feature #124] Remove a user from shared folder

[Feature #1259] Enhancement to workspace item history

[Bug #1373] Fixed: breadcrumb slowness on file upload make upload of files on parent folders

[Incident #1338] Fixed: Workspace and ownership ... something is wrong

[Bug: #1459] Fixed: get link disabled


## [v6.9.0] - 2015-10-30

[Bug #718] Fixed breadcrumb path

[Bug #546] Fixed bug

[Bug #531] Fixed issue on create folder

[Feature #429] Realized. Integrated with workspace-uploader

[Bug #1270] Fixed. Workspace improvements: many rpc calls after an delete multiple


## [v6.8.0] - 2015-07-06

[Feature #129] Porting to HL 2.0

[Feature #331] Public Link updated to show short link and complete link


## [v6.7.2] - 2015-04-15

Integrated with Contact Edit Permissions


## [v6.7.1] - 2014-10-29

Added user storage usage

Added notifications for: set folder Administrator, delete shared folder


## [v6.6.7] - 2014-09-01

Support Ticket #813: Broken Breadcrumb for long path

Ticket #3053: Load current ACL stored to Shared Folder


## [v6.6.6] - 2014-07-02

[#2798] Sharing Panel usability improved

Completed activity to https://support.social.isti.cnr.it/ticket/126


## [v6.6.5] - 2014-06-04

Added Trash: https://issue.imarine.research-infrastructures.eu/ticket/2497

Updated pom to support new portal configuration (gcube release 3.2)


## [v6.6.4] - 2014-05-22

Fixed: https://support.d4science.research-infrastructures.eu/ticket/843

Added: item number in grid and trash. See: https://issue.imarine.research-infrastructures.eu/ticket/282


## [v6.6.2] - 2014-3-17

Management of My Special Folder

Added change permissions to VRE shared folder


## [v6.6.0] - 2014-2-07

[#2634] Workspace: support for setting permissions over shared folders

[#2633] Workspace: support for VRE Shared Folders

[#2290] Worskspace history operations: should support the session validation


## [v6.5.1] - 2013-12-12

Changed several labels into Logger


## [v6.5.0] - 2013-10-21

#Ticket 2223. This project was enhancements to gwt 2.5.1

GCF dependency was removed


## [v6.4.0] - 2013-09-16

Provide support for public link, Related ticket: #1993

Link sharing and public link: generate a human-readable URL via URL shortening, Related ticket: #1921

Fixed bug on shared links, Ticket #630


## [v6.3.0] - 2013-07-08

Provide support for share link, Related ticket: #1504

Edit description on sharing, Related ticket: #1822

Bugs fixed, Related Tickets: #628, #633, #630


## [v6.2.0] - 2013-05-29

Provide support for accounting, related ticket: #1752

Enable notification for file upload in shared folder, related ticket: #1732


## [v6.1.0] - 2013-04-19

Workspace portlet was enhanced to meet the requests coming from the User Community

Related tickets: #1500, #1498, #320, #1487, #1499, #1501, #1497, #1536


## [v6.0.0] - 2013-03-05

[#1247] The workspace tree was mavenized

[#230], [#205] The Workspace environment now supports the sharing of folder between users


## [v5.1.0] - 2012-09-21

Old Send Message removed, added support for new mail sender

Bug fixed: upload file/archive


## [v5.0.0] - 2012-05-04

[#216] New version of Workspace Tree is developed using the GXT framework

[#216] New tree is asynchronous

[#216] Reviews tree GUI and adding new features


## [v4.3.0] - 2011-09-01

[#1740]: ICIS / Download csv / added extension .xlw


## [v4.2.0] - 2011-07-01

[#1555]: WorkflowTemplate and WorkflowReport required in Workspace Portlet


## [v4.1.0] - 2011-05-06

Synch with others changes in HomeLibrary

GWT 2.2.0


## [v4.0.0] - 2011-02-07

Removed Workspace and Basket type, replaced with folder option

Synch with others changes in HomeLibrary


## [v3.2.1] - 2010-11-09

Added more checks on GWT model construction


## [v3.2.0] - 2010-10-22

Enabled details panel for Report, ReportTemplate and AquaMapsItem workspace items


## [v3.1.0] - 2010-09-03

[#36] ICIS / Workspace / Upload an archive

Merged ThumbnailServlet and ImageServlet

[#774]: Workspace portlet tree - Open Report / Template redirect link not working


## [v3.0.0] - 2010-07-16

Ported to GWT 2.0

Updated project structure to WebPortlet

Update to LifeRay portal


## [v2.6.0] - 2010-05-14

[#424]: Workspace code refactoring an enanchement

refactored code

uniformed icons, no more specialized action icons

added more informations on items like AquaMapsItem, Report and Report Template


## [v2.5.0] - 2010-01-29

gcube release 1.7.0


## [v2.4.0] - 2009-11-30

gcube release 1.6.0


## [v2.3.0] - 2009-11-16

gcube release 1.5.0


## [v2.2.0] - 2009-10-16

gcube release


## [v2.1.1] - 2009-07-29

gcube release 1.2.2


## [v2.1.0] - 2009-07-14

gcube release 1.2.0


## [v2.0.0] - 2009-05-19

gcube release 1.2.0 rc1


## [v1.1.2] - 2009-01-12

first release
