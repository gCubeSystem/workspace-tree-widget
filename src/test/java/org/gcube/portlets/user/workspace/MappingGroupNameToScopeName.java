package org.gcube.portlets.user.workspace;

import org.gcube.common.storagehubwrapper.server.StorageHubClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MappingGroupNameToScopeName {

	private static Logger logger = LoggerFactory.getLogger(MappingGroupNameToScopeName.class);

	public static void main(String[] args) {

		// VRE
		String context = "d4science.research-infrastructures.eu-D4OS-FAO-Tuna_Atlas";
		String rootName = "/d4science.research-infrastructures.eu";
		String vreName = "FAO-Tuna_Atlas";
//		
//		//VO
//		String context = "d4science.research-infrastructures.eu-D4OS";
//		String rootName = "/d4science.research-infrastructures.eu";
//		String vreName = null;

		// ROOT
//		String context = "d4science.research-infrastructures.eu";
//		String rootName = "/d4science.research-infrastructures.eu";
//		String vreName = null;

		System.out.println(getScopeFromVREGroupName(context, rootName, vreName));
	}

	/**
	 * Gets the scope from VRE group name.
	 *
	 * @param context the context
	 * @return the scope from VRE group name. Eg. with the input
	 *         'gcube-devNext-NextNext' returns '/gcube/devNext/NextNext'
	 */
	public static String getScopeFromVREGroupName(String context, String infrastructurName, String vreName) {
//		String entireScopeName = context.replaceAll("^/(.*)/?$", "$1").replaceAll("-", "/");
//		return entireScopeName;

		if (vreName == null)
			vreName = "";

		logger.debug("vreName: " + vreName);

		if (infrastructurName.startsWith("/"))
			infrastructurName = infrastructurName.substring(1, infrastructurName.length());

		logger.debug("infrastructurName: " + infrastructurName);

		String voGroup = context.replaceAll(infrastructurName, "");
		if (vreName != null) {
			voGroup = voGroup.replaceAll(vreName, "").replaceAll("-", "");
		}
		String voName = voGroup.replaceAll("-", "");
		logger.debug("voName: " + voName);

		StringBuilder theScopeBuilder = new StringBuilder();
		theScopeBuilder.append("/" + infrastructurName);
		if (voName != null && !voName.isEmpty()) {
			theScopeBuilder.append("/" + voName);
		}
		if (vreName != null && !vreName.isEmpty()) {
			theScopeBuilder.append("/" + vreName);
		}
		String theScope = theScopeBuilder.toString();
		logger.info("Built scope: " + theScope);
		return theScope;
	}
}
