package org.gcube.portlets.user.workspace.junit;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.workspace.server.reader.ApplicationProfileReader;

public class WEA_AP_Reader {

	public static String SCOPE = "/d4science.research-infrastructures.eu";
	public static String resourceName = "Workspace-Explorer-App";
	public static String appID = "org.gcube.portlets.user.workspaceexplorerapp.server.WorkspaceExplorerAppServiceImpl";

	// @Test
	public void readAP() {

		try {
			// ScopeProvider.instance.set("/d4science.research-infrastructures.eu/D4Research/Limnodata");
			ScopeProvider.instance.set(SCOPE);
			ApplicationProfileReader ap = new ApplicationProfileReader(resourceName, appID);
			System.out.println("ApplicationProfile found: " + ap.readProfileFromInfrastrucure());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
