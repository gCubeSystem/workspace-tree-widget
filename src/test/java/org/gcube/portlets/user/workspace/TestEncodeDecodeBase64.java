/**
 *
 */
package org.gcube.portlets.user.workspace;

import org.gcube.common.encryption.StringEncrypter;
import org.gcube.portlets.user.workspace.server.util.StringUtil;


/**
 * The Class TestEncodeDecodeBase64.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa{@literal @}isti.cnr.it
 * Sep 19, 2016
 */
public class TestEncodeDecodeBase64 {

	public static final String CHAR_FOR_SEARCHING_WITH_EXACTLY_MATCH = "\""; //is double quote
	static final String SCOPE = "/gcube";
	private static final String JCR_WILDCARD_TO_SEARCH = "%";
//	static String folderId = "e7b6bc31-8c35-4398-a7fd-492e391e17d2";
	static String folderId = "ce4866ee-8079-4acf-bcd6-1c9dd786eb73";

	static String encrypted="";
	static String encoded = "";
	static String decoded = "";
	static String decrypted = "";

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		try {
			
			String[] listTexts = new String[]{"data","data%", "\"mydata\""};
			
			for (String text : listTexts) {
				System.out.println("\n\nInput searched text is: "+text);
				String toSearch = text;
				if(toSearch.startsWith(CHAR_FOR_SEARCHING_WITH_EXACTLY_MATCH) && toSearch.endsWith(CHAR_FOR_SEARCHING_WITH_EXACTLY_MATCH)){
					System.out.println("Detected intent of exactly mathing, the searched text is between "+CHAR_FOR_SEARCHING_WITH_EXACTLY_MATCH+" char");
					toSearch = toSearch.substring(1,toSearch.length()-1);
					System.out.println("the searched text for exactly mathing is: "+toSearch);
				}else {
					toSearch = String.format("%s%s%s", JCR_WILDCARD_TO_SEARCH,toSearch,JCR_WILDCARD_TO_SEARCH);
					System.out.println("prepending and appending the wildcard "+JCR_WILDCARD_TO_SEARCH+", changed the input text to: "+toSearch);
				}
				System.out.println("backend searched text is: "+toSearch);
			}
			
			/*
			
			ScopeProvider.instance.set(SCOPE);
			System.out.println("Folder Id: "+folderId);
			encode();
			decode();

			if(decrypted.compareTo(folderId)==0)
				System.out.println("Encrypt/Decript works!");
			else
				System.out.println("Encrypt/Decript doesn't work!");
			*/
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Encode.
	 *
	 * @throws Exception the exception
	 */
	private static void encode() throws Exception {

		encrypted = StringEncrypter.getEncrypter().encrypt(folderId);
		System.out.println("Encrypted folder Id: "+encrypted);
		encoded = StringUtil.base64EncodeStringURLSafe(encrypted);
		System.out.println("Encoded folder Id: "+encoded);

	}

	/**
	 * Decode.
	 *
	 * @throws Exception the exception
	 */
	private static void decode() throws Exception {

		decoded = StringUtil.base64DecodeString(encoded);
		System.out.println("Decoded folder Id: "+decoded);
		decrypted = StringEncrypter.getEncrypter().decrypt(decoded);
		System.out.println("Decrypted folder Id: "+decrypted);
	}
}
