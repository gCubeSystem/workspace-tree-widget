package org.gcube.portlets.user.workspace.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface WebDavUrlEventHandler extends EventHandler {
	void onClickWebDavUrl(WebDavUrlEvent webDavUrlEvent);
}