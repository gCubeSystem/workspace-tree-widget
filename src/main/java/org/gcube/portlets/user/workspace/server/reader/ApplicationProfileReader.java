package org.gcube.portlets.user.workspace.server.reader;

import static org.gcube.resources.discovery.icclient.ICFactory.client;

import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.gcube.common.resources.gcore.utils.XPathHelper;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.common.scope.impl.ScopeBean.Type;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.Query;
import org.gcube.resources.discovery.client.queries.impl.QueryBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

/**
 * The Class ApplicationProfileReader.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa{@literal @}isti.cnr.it
 *         Sep 14, 2016
 */
public class ApplicationProfileReader {

	/**
	 *
	 */
	public static final String SECONDARY_TYPE = "ApplicationProfile";
	public static final String WORKSPACE_EXPLORER_APP_NAME = "Workspace-Explorer-App";

	protected static final String RESOURCE_PROFILE_BODY_END_POINT_URL_TEXT = "/Resource/Profile/Body/EndPoint/URL/text()";
	protected static final String RESOURCE_PROFILE_BODY_END_POINT_SCOPE_TEXT = "/Resource/Profile/Body/EndPoint/Scope/text()";
	protected static final String RESOURCE_PROFILE_BODY_TEXT = "/Resource/Profile/Body/text()";
	protected static final String RESOURCE_PROFILE_BODY_THUMBNAIL_URL_TEXT = "/Resource/Profile/Body/ThumbnailURL/text()";
	protected static final String RESOURCE_PROFILE_BODY_APP_ID_TEXT = "/Resource/Profile/Body/AppId/text()";
	protected static final String RESOURCE_PROFILE_DESCRIPTION_TEXT = "/Resource/Profile/Description/text()";
	protected static final String RESOURCE_PROFILE_NAME_TEXT = "/Resource/Profile/Name/text()";

	private Logger logger = LoggerFactory.getLogger(ApplicationProfileReader.class);
	private String secondaryType;
	private String scope;
	private String resourceName;
	private String appID;

	public enum APPLICATION_PROFILE_ITEM {

		SCOPE("/Scope/text()"), URL("/URL/text()");

		private String xPath;

		APPLICATION_PROFILE_ITEM(String xpath) {
			this.xPath = xpath;
		}

		public String getXPath() {
			return xPath;
		}
	}

	/**
	 * Instantiates a new application profile reader.
	 *
	 * @param resourceName the resource name
	 * @param appID        the app id
	 * @throws Exception the exception
	 */
	public ApplicationProfileReader(String resourceName, String appID) throws Exception {

		this.resourceName = resourceName;
		this.appID = appID;
		this.secondaryType = SECONDARY_TYPE;
		this.scope = ScopeProvider.instance.get();
	}

	/**
	 * this method looks up the generic resource among the ones available in the
	 * infrastructure using scope provider {@link ScopeProvider.instance.get()}
	 * resource name {@value #WORKSPACE_EXPLORER_APP_NAME} and secondaryType
	 * {@value #SECONDARY_TYPE}
	 *
	 * @return the applicationProfile profile
	 */
	/**
	 * this method looks up the applicationProfile profile among the ones available
	 * in the infrastructure.
	 *
	 * @return the applicationProfile profile
	 */
	public ApplicationProfile readProfileFromInfrastrucure() {

		ApplicationProfile appProf = new ApplicationProfile();
		String queryString = getGcubeGenericQueryString(secondaryType, appID);

		try {

			if (scope == null)
				throw new Exception("Scope is null, set scope into ScopeProvider");

			logger.info("Trying to fetch " + SECONDARY_TYPE + " in the scope: " + scope + ", SecondaryType: "
					+ secondaryType + ", AppId: " + appID);
			Query q = new QueryBox(queryString);
			DiscoveryClient<String> client = client();
			List<String> appProfile = client.submit(q);

			if (appProfile == null || appProfile.size() == 0)
				throw new ApplicationProfileNotFoundException("Generic Resource with SecondaryType: " + secondaryType
						+ ", AppId: " + appID + " is not registered in the scope: " + scope);
			else {
				String elem = appProfile.get(0);
				DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Node node = docBuilder.parse(new InputSource(new StringReader(elem))).getDocumentElement();
				XPathHelper helper = new XPathHelper(node);

				List<String> currValue = null;
				currValue = helper.evaluate(RESOURCE_PROFILE_NAME_TEXT);
				if (currValue != null && currValue.size() > 0) {
					appProf.setName(currValue.get(0));
				} else
					throw new ApplicationProfileNotFoundException(
							"Your " + SECONDARY_TYPE + " NAME was not found in the generic resource");

				currValue = helper.evaluate(RESOURCE_PROFILE_DESCRIPTION_TEXT);
				if (currValue != null && currValue.size() > 0) {
					appProf.setDescription(currValue.get(0));
				} else
					logger.warn("No Description exists for " + appProf.getName());

				currValue = helper.evaluate(RESOURCE_PROFILE_BODY_APP_ID_TEXT);
				if (currValue != null && currValue.size() > 0) {
					appProf.setKey(currValue.get(0));
				} else
					throw new ApplicationProfileNotFoundException("Your " + SECONDARY_TYPE
							+ " ID was not found in the generic resource, consider adding <AppId> element in <Body>");

				currValue = helper.evaluate(RESOURCE_PROFILE_BODY_THUMBNAIL_URL_TEXT);
				if (currValue != null && currValue.size() > 0) {
					appProf.setImageUrl(currValue.get(0));
				} else {
					logger.warn("Null or empty <ThumbnailURL> element in <Body> of " + appProf.getName());
				}

				currValue = helper
						.evaluate("/Resource/Profile/Body/EndPoint[Scope='" + scope.toString() + "']/Scope/text()");

				appProf = readEndPointForScopeWithRetry(helper, scope, APPLICATION_PROFILE_ITEM.SCOPE, appProf);

				appProf = readEndPointForScopeWithRetry(helper, scope, APPLICATION_PROFILE_ITEM.URL, appProf);

				logger.debug("returning: " + appProf);
				return appProf;
			}

		} catch (Exception e) {
			logger.error("Error while trying to fetch applicationProfile profile from the infrastructure", e);
			return null;
		} finally {
			/*
			 * if(originalScope!=null && !originalScope.isEmpty()){
			 * ScopeProvider.instance.set(originalScope);
			 * logger.info("scope provider setted to orginal scope: "+originalScope); }else{
			 * ScopeProvider.instance.reset(); logger.info("scope provider reset"); }
			 */
		}

	}

	private ApplicationProfile readEndPointForScopeWithRetry(XPathHelper helper, String scope,
			APPLICATION_PROFILE_ITEM searchItem, ApplicationProfile appProf)
			throws ApplicationProfileNotFoundException {

		String xPathToQuery = "/Resource/Profile/Body/EndPoint[Scope='" + scope + "']" + searchItem.getXPath();
		logger.debug("Identifying path with current scope: " + xPathToQuery);
		List<String> currValue = helper.evaluate(xPathToQuery);

		String queryResult = null;

		if (currValue == null || currValue.isEmpty()) {
			logger.warn("In the " + SECONDARY_TYPE + " with name " + appProf.getName() + " the xPath " + xPathToQuery
					+ " returned with no results");

			ScopeBean scopeBean = new ScopeBean(scope);
			if (scopeBean.is(Type.VRE) || scopeBean.is(Type.VO)) {
				String pathSeparator = "/";
				String[] components = scope.split(pathSeparator);
				scope = pathSeparator + components[1];
				logger.info("The scope "+scopeBean.toString()+" is of kind " + Type.VRE + " or " + Type.VO
						+ ". Retry in action - going to search the default <EndPoint> with the root scope: " + scope);

				xPathToQuery = "/Resource/Profile/Body/EndPoint[Scope='" + scope + "']" + searchItem.getXPath();
				logger.debug("Identifying path with root scope: " + xPathToQuery);
				currValue = helper.evaluate(xPathToQuery);

				if (currValue == null || currValue.isEmpty()) {
					logger.warn("In the " + SECONDARY_TYPE + " with name " + appProf.getName() + " the xPath " + xPathToQuery
							+ " returned with no results");
				}

			}
		}

		if (currValue == null || currValue.isEmpty()) {
			throw new ApplicationProfileNotFoundException("Your ApplicationProfile in the scope " + scope
					+ " have not an <EndPoint> accessible, consider adding <EndPoint><Scope></Scope> element in <Body>");
		}

		queryResult = currValue.get(0);
		logger.info("The xPath: " + xPathToQuery + " returned with the result: " + queryResult);

		switch (searchItem) {
		case SCOPE:
			appProf.setScope(currValue.get(0));
			break;
		case URL:
			appProf.setUrl(currValue.get(0));
			break;
		default:
			break;
		}

		return appProf;

	}

	/**
	 * Gets the gcube generic xPath string.
	 *
	 * @param secondaryType the secondary type
	 * @param appId         the app id
	 * @return the gcube generic xPath string
	 */
	public static String getGcubeGenericQueryString(String secondaryType, String appId) {

		return "for $profile in collection('/db/Profiles/GenericResource')//Resource "
				+ "where $profile/Profile/SecondaryType/string() eq '" + secondaryType
				+ "' and  $profile/Profile/Body/AppId/string() " + " eq '" + appId + "'" + "return $profile";
	}

	/**
	 * Gets the secondary type.
	 *
	 * @return the secondary type
	 */
	public String getSecondaryType() {
		return secondaryType;
	}

	/**
	 * Gets the scope.
	 *
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * Gets the resource name.
	 *
	 * @return the resource name
	 */
	public String getResourceName() {
		return resourceName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("ApplicationProfileReader [secondaryType=");
		builder.append(secondaryType);
		builder.append(", scope=");
		builder.append(scope);
		builder.append(", resourceName=");
		builder.append(resourceName);
		builder.append(", appID=");
		builder.append(appID);
		builder.append("]");
		return builder.toString();
	}

}
