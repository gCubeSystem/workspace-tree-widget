/**
 * 
 */
package org.gcube.portlets.user.workspace.server.property;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa{@literal @}isti.cnr.it
 * Jun 26, 2013
 *
 */
public class PropertyFileNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3220297615560720000L;
	
	public PropertyFileNotFoundException() {
		super();
	}

}
