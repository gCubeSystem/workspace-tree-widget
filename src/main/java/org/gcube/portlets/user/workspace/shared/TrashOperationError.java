/**
 * 
 */
package org.gcube.portlets.user.workspace.shared;

import java.io.Serializable;

import org.gcube.portlets.user.workspace.client.model.FileTrashedModel;

/**
 * The Class TrashOperationContent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy) May 18, 2020
 */
public class TrashOperationError implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5792786687183945714L;
	private FileTrashedModel trashedFile;
	private WorkspaceTrashOperation operation;
	private String erroMsg;

	public TrashOperationError() {

	}

	public TrashOperationError(FileTrashedModel trashedFile, WorkspaceTrashOperation operation, String erroMsg) {
		super();
		this.trashedFile = trashedFile;
		this.operation = operation;
		this.erroMsg = erroMsg;
	}

	public FileTrashedModel getTrashedFile() {
		return trashedFile;
	}

	public void setTrashedFile(FileTrashedModel trashedFile) {
		this.trashedFile = trashedFile;
	}

	public WorkspaceTrashOperation getOperation() {
		return operation;
	}

	public void setOperation(WorkspaceTrashOperation operation) {
		this.operation = operation;
	}

	public String getErroMsg() {
		return erroMsg;
	}

	public void setErroMsg(String erroMsg) {
		this.erroMsg = erroMsg;
	}

	@Override
	public String toString() {
		return "TrashOperationError [trashedFile=" + trashedFile + ", operation=" + operation + ", erroMsg=" + erroMsg
				+ "]";
	}
	
}
