/**
 * 
 */
package org.gcube.portlets.user.workspace.shared;

import java.io.Serializable;
import java.util.List;

import org.gcube.portlets.user.workspace.client.model.FileTrashedModel;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa{@literal @}isti.cnr.it
 * Feb 17, 2014
 *
 */
public class TrashContent implements Serializable{
	
	
	private static final long serialVersionUID = -7428752149892396573L;
	
	
	List<FileTrashedModel> trashContent;
	List<TrashOperationError> listErrors;
	
	public TrashContent() {
	}

	public TrashContent(List<FileTrashedModel> trashContent, List<TrashOperationError> listErrors) {
		this.trashContent = trashContent;
		this.listErrors = listErrors;
	}

	public List<FileTrashedModel> getTrashContent() {
		return trashContent;
	}

	public void setTrashContent(List<FileTrashedModel> trashContent) {
		this.trashContent = trashContent;
	}

	public List<TrashOperationError> getListErrors() {
		return listErrors;
	}

	public void setListErrors(List<TrashOperationError> listErrors) {
		this.listErrors = listErrors;
	}

	@Override
	public String toString() {
		return "TrashContent [trashContent=" + trashContent + ", listErrors=" + listErrors + "]";
	}
	
	
}
