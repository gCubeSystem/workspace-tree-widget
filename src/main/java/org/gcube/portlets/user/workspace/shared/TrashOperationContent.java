/**
 * 
 */
package org.gcube.portlets.user.workspace.shared;

import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Class TrashOperationContent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * May 18, 2020
 */
public class TrashOperationContent extends TrashContent{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7825694364945907231L;
	private List<String> listTrashIds;
	private List<String> listDestinationFolderIds;
	private WorkspaceTrashOperation operation;

	/**
	 * Instantiates a new trash operation content.
	 */
	public TrashOperationContent() {
	}

	/**
	 * Instantiates a new trash operation content.
	 *
	 * @param operation the operation
	 * @param listTrashIds the list trash ids
	 */
	public TrashOperationContent(WorkspaceTrashOperation operation, List<String> listTrashIds) {
		this.listTrashIds = listTrashIds;
		this.operation = operation;
	}
	
	/**
	 * Instantiates a new trash operation content.
	 *
	 * @param listTrashIds the list trash ids
	 * @param listDestinationFolderIds the list destination folder ids
	 * @param operation the operation
	 */
	public TrashOperationContent(List<String> listTrashIds, List<String> listDestinationFolderIds,
			WorkspaceTrashOperation operation) {
		super();
		this.listTrashIds = listTrashIds;
		this.listDestinationFolderIds = listDestinationFolderIds;
		this.operation = operation;
	}

	/**
	 * Sets the list trash ids.
	 *
	 * @param listTrashIds the new list trash ids
	 */
	public void setListTrashIds(List<String> listTrashIds) {
		this.listTrashIds = listTrashIds;
	}

	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	public WorkspaceTrashOperation getOperation() {
		return operation;
	}

	/**
	 * Sets the operation.
	 *
	 * @param operation the new operation
	 */
	public void setOperation(WorkspaceTrashOperation operation) {
		this.operation = operation;
	}

	/**
	 * Gets the list trash ids.
	 *
	 * @return the list trash ids
	 */
	public List<String> getListTrashIds() {
		return listTrashIds;
	}
	
	/**
	 * Gets the list destination folder ids.
	 *
	 * @return the list destination folder ids
	 */
	public List<String> getListDestinationFolderIds() {
		return listDestinationFolderIds;
	}

	@Override
	public String toString() {
		return "TrashOperationContent [listTrashIds=" + listTrashIds + ", listDestinationFolderIds="
				+ listDestinationFolderIds + ", operation=" + operation + "]";
	}

	
	
}
