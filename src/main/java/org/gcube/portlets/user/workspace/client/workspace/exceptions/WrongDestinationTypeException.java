/**
 * 
 */
package org.gcube.portlets.user.workspace.client.workspace.exceptions;

/**
 * @author Federico De Faveri defaveriAtisti.cnr.it
 *
 */
public class WrongDestinationTypeException extends Exception {

	private static final long serialVersionUID = 693144159858314762L;

	public WrongDestinationTypeException(String message) {
		super(message);
	}
	
	

}
