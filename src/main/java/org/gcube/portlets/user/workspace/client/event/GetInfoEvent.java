package org.gcube.portlets.user.workspace.client.event;

import org.gcube.portlets.user.workspace.client.interfaces.EventsTypeEnum;
import org.gcube.portlets.user.workspace.client.interfaces.GuiEventInterface;
import org.gcube.portlets.user.workspace.client.model.FileModel;

import com.google.gwt.event.shared.GwtEvent;

/**
 * 
 * @author Francesco Mangiacrapa francesco.mangiacrapa{@literal @}isti.cnr.it
 *
 */
public class GetInfoEvent extends GwtEvent<GetInfoEventHandler> implements GuiEventInterface{
  public static Type<GetInfoEventHandler> TYPE = new Type<GetInfoEventHandler>();

  private FileModel targetFile = null;
  
	public GetInfoEvent(FileModel target) {
		this.targetFile = target;
	}

	@Override
	public Type<GetInfoEventHandler> getAssociatedType() {
		return TYPE;
	}
	
	@Override
	protected void dispatch(GetInfoEventHandler handler) {
		handler.onGetInfo(this);
		
	}

	public FileModel getSourceFile() {
		return targetFile;
	}

	@Override
	public EventsTypeEnum getKey() {
		return EventsTypeEnum.GET_DETAILS_FOR_ITEM;
	}
}