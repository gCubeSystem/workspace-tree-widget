package org.gcube.portlets.user.workspace.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface RenameItemEventHandler extends EventHandler {
	void onRenameItem(RenameItemEvent event);
}
