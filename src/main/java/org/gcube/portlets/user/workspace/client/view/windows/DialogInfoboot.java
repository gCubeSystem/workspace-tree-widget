package org.gcube.portlets.user.workspace.client.view.windows;

import java.util.List;

import org.gcube.portlets.user.workspace.client.AppControllerExplorer;
import org.gcube.portlets.user.workspace.client.model.FileModel;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.ModalFooter;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class DialogInfoboot extends Composite {

	private static DialogInfobootUiBinder uiBinder = GWT.create(DialogInfobootUiBinder.class);

	public static final String EMPTY = "empty";
	public static final String UNKNOWN = "unknown";

	private Modal modalBox = new Modal();

	@UiField
	TextBox textName;

	@UiField
	TextBox textLocation;

	@UiField
	TextBox textIdentifier;

	@UiField
	TextBox textDescription;

	@UiField
	TextBox textType;

	@UiField
	TextBox textCategory;

	@UiField
	TextBox textOwner;

	@UiField
	TextBox textCreated;

	@UiField
	TextBox textLastModified;

	@UiField
	TextBox textSize;

	@UiField
	TextBox textSharingMode;


	interface DialogInfobootUiBinder extends UiBinder<Widget, DialogInfoboot> {
	}

	public DialogInfoboot(FileModel item) {
		initWidget(uiBinder.createAndBindUi(this));
		/*typeIcon.setIcon(IconType.FILE);*/
		modalBox.setTitle("Info for "+item.getName());

		modalBox.setAnimation(true);
        
		textName.setValue(item.getName());
		
		textIdentifier.setValue(item.getIdentifier());
		textType.setValue(item.getType());

		String sharingMode = "PRIVATE";

		if (item.isShared()) {
			if(item.isSpecialFolder()) 
				sharingMode = "VRE FOLDER";
			else sharingMode="SHARED"; 
		}

		textSharingMode.setValue(sharingMode);
		
		//TODO: Location can be a property of FileModel
		if (item.isRoot())
			textLocation.setValue("/");
		else
			loadLocation(item.getIdentifier());
		
		if (item.getShortcutCategory() != null)
			textCategory.setValue(item.getShortcutCategory().getValue());
		
		ModalFooter modalFooter = new ModalFooter();
		final Button buttClose = new Button("Close");

		buttClose.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				modalBox.hide();
			}
		});

		modalFooter.add(buttClose);
		modalBox.add(this);
		modalBox.add(modalFooter);
		modalBox.show();
	}

	/*private String getFormattedSize(long value) {

		if (value > 0) {
			double kb = value / 1024;
			if (kb < 1)
				kb = 1;
			return number.format(kb);
		} else if (value == 0) {
			return EMPTY;
		} else
			return "";
	}*/

	private void loadLocation(String itemId) {

		AppControllerExplorer.rpcWorkspaceService.getListParentsByItemIdentifier(itemId, false,
				new AsyncCallback<List<FileModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("failure get list parents by item identifier " + caught);
				textLocation.setValue(UNKNOWN);
			}

			@Override
			public void onSuccess(List<FileModel> result) {

				String location = "";
				if (result != null) {
					for (FileModel fileModel : result) {
						if (fileModel != null)
							location += "/" + fileModel.getName();
					}
				}
				if (location.isEmpty())
					location = "/";

				textLocation.setValue(location);
			}
		});

	}

}
