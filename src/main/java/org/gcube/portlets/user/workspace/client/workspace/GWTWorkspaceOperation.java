/**
 * 
 */
package org.gcube.portlets.user.workspace.client.workspace;

/**
 * @author Federico De Faveri defaveriAtisti.cnr.it
 *
 */
public enum GWTWorkspaceOperation {
	
	DELETE,
	RENAME;

}
